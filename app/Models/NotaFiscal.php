<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class NotaFiscal extends Model
{
    use HasFactory;

    protected $table = 'compra_nota_fiscal';

    protected $fillable = [
        'numero_nota_fiscal',
        'fornecedor_id',
        'comprado_at',
    ];

    protected $casts = [
        'comprado_at' => 'datetime'
    ];

    public function fornecedor(): BelongsTo
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function ativos(): BelongsToMany
    {
        return $this->belongsToMany(Ativo::class, 'compra_ativos', 'nota_fiscal_id', 'ativo_id');
    }
}
