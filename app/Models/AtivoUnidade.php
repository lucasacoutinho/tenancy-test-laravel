<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AtivoUnidade extends Pivot
{
    protected $table = 'tenant_ativos';

    protected $fillable = [
        'ativo_id',
        'tenant_id',
        'quantidade',
    ];

    public static function boot()
    {
        parent::boot();

        static::updating(function($model)  {
            if($model->quantidade === 0){
                $model->delete();
            }
        });
    }

    public function ativo(): BelongsTo
    {
        return $this->belongsTo(Ativo::class, 'ativo_id');
    }

    public function unidade(): BelongsTo
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }
}
