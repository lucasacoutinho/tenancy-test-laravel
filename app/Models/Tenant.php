<?php

namespace App\Models;

use Stancl\Tenancy\Contracts\TenantWithDatabase;
use Stancl\Tenancy\Database\Concerns\HasDomains;
use Stancl\Tenancy\Database\Concerns\HasDatabase;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Stancl\Tenancy\Database\Models\Tenant as BaseTenant;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tenant extends BaseTenant implements TenantWithDatabase
{
    use HasDatabase;
    use HasDomains;

    public function ativos(): BelongsToMany
    {
        return $this->belongsToMany(Ativo::class, 'tenant_ativos', 'tenant_id', 'ativo_id')->withPivot(['quantidade']);
    }

    public function trasnferenciasEnviadas(): HasMany
    {
        return $this->hasMany(Transferencia::class, 'unidade_origem');
    }

    public function transferenciasRecebidas(): HasMany
    {
        return $this->hasMany(Transferencia::class, 'unidade_destino');
    }

    public function usuarios(): HasMany
    {
        return $this->hasMany(User::class, 'tenant_id');
    }
}
