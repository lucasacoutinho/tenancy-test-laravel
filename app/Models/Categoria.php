<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Categoria extends Model
{
    use HasFactory;

    protected $table = 'categorias';

    protected $fillable = [
        'titulo'
    ];

    public function ativos(): BelongsToMany
    {
        return $this->belongsToMany(Ativo::class, 'ativo_categorias', 'categoria_id', 'ativo_id');
    }
}
