<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Fornecedor extends Model
{
    use HasFactory;

    protected $table = 'fornecedores';

    protected $fillable = [
        'titulo'
    ];

    public function notasFiscais(): HasMany
    {
        return $this->hasMany(NotaFiscal::class, 'fornecedor_id');
    }
}
