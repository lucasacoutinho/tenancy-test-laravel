<?php

namespace App\Http\Requests\Central\Permissao;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesPermissoes;
use Illuminate\Foundation\Http\FormRequest;

class PermissaoStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesPermissoes::STORE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'name' => Str::lower($this->nome),
        ]);
    }

    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255', Rule::unique('permissions', 'name')],
        ];
    }
}
