<?php

namespace App\Http\Requests\Central\Ativo;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesAtivos;
use Illuminate\Foundation\Http\FormRequest;

class AtivoStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesAtivos::STORE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'titulo' => trim(Str::lower($this->titulo)),
        ]);
    }

    public function rules()
    {
        return [
            'titulo'             => ['required', 'string', 'max:255'],
            'descricao'          => ['required', 'string'],
            'categorias'         => ['nullable', 'array'],
            'categorias.*'       => ['required', 'integer', Rule::exists('categorias', 'id')],
            'n_patrimonio'       => ['nullable', 'string', 'max:255'],
            'n_serie'            => ['nullable', 'string', 'max:255'],
            'n_etiqueta_servico' => ['nullable', 'string', 'max:255'],
        ];
    }
}
