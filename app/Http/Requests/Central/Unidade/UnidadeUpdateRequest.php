<?php

namespace App\Http\Requests\Central\Unidade;

use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesUnidades;
use Illuminate\Foundation\Http\FormRequest;

class UnidadeUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesUnidades::UPDATE);
    }

    public function rules()
    {
        return [
            'unidade' => ['filled', 'string', 'min:3', Rule::unique('tenants', 'id')],
        ];
    }

    public function validated(): array
    {
        if ($this->has('unidade')) {
            return array_merge(parent::validated(), ['id' => $this->input('unidade')]);
        }

        return parent::validated();
    }
}
