<?php

namespace App\Http\Requests\Central\Compra;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Domain\Permissoes\PermissoesNotasFiscais;

class NotaFiscalUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesNotasFiscais::UPDATE);
    }

    public function rules()
    {
        return [
            'numero_nota_fiscal' => ['filled', 'string', 'size:44'],
            'fornecedor_id'      => ['filled', 'integer', Rule::exists('fornecedores', 'id')],
            'comprado_at'        => ['filled', 'date', 'date_format:Y-m-d'],
        ];
    }
}
