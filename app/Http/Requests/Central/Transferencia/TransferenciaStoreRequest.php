<?php

namespace App\Http\Requests\Central\Transferencia;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Central\Transferencia\Estoque;
use Domain\Permissoes\PermissoesTransferencias;

class TransferenciaStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesTransferencias::STORE);
    }

    public function rules()
    {
        return [
            'ativo_id'   => ['required', 'integer', Rule::exists('ativos', 'id')],
            'unidade'    => ['required', 'string', Rule::exists('tenants', 'id')],
            'quantidade' => ['required', 'integer', 'min:1', new Estoque($this->ativo_id)],
        ];
    }


    public function validated(): array
    {
        if ($this->has('unidade')) {
            return array_merge(parent::validated(), ['unidade_destino' => $this->input('unidade')]);
        }

        return parent::validated();
    }
}
