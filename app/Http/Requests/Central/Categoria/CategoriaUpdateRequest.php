<?php

namespace App\Http\Requests\Central\Categoria;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesCategorias;
use Illuminate\Foundation\Http\FormRequest;

class CategoriaUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesCategorias::UPDATE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'titulo' => trim(Str::lower($this->titulo)),
        ]);
    }

    public function rules()
    {
        return [
            'titulo' => ['filled', 'string', 'max:255', Rule::unique('categorias', 'titulo')->ignore($this->categoria)]
        ];
    }
}
