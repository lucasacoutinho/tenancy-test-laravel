<?php

namespace App\Http\Requests\Central\Funcao;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesFuncoes;
use Illuminate\Foundation\Http\FormRequest;

class FuncaoUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesFuncoes::UPDATE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'name'        => Str::ucfirst(Str::lower($this->input('nome'))),
            'permissions' => $this->input('permissoes'),
        ]);
    }

    public function rules()
    {
        return [
            'name'          => ['filled', 'string', 'max:255', Rule::unique('roles', 'name')->ignore($this->funcao)],
            'permissions'   => ['nullable', 'array'],
            'permissions.*' => ['required', 'integer', Rule::exists('permissions', 'id')],
        ];
    }
}
