<?php

namespace App\Http\Requests\Central\Fornecedor;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Domain\Permissoes\PermissoesFornecedores;

class FornecedorUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesFornecedores::UPDATE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'titulo' => trim(Str::lower($this->titulo)),
        ]);
    }

    public function rules()
    {
        return [
            'titulo' => ['filled', 'string', 'max:255', Rule::unique('fornecedores', 'titulo')->ignore($this->fornecedor)]
        ];
    }
}
