<?php

namespace App\Http\Requests\Central\Usuario;

use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesUsuarios;
use Illuminate\Foundation\Http\FormRequest;

class UsuarioUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesUsuarios::UPDATE);
    }

    public function rules()
    {
        return [
            'name'      => ['filled', 'string', 'max:255'],
            'email'     => ['filled', 'string', 'email', 'max:255', Rule::unique('users', 'email')->where('tenant_id', $this->input('unidade'))->ignore($this->usuario)],
            'unidade'   => ['nullable', 'string',  Rule::exists('tenants', 'id')],
            'funcao'    => ['nullable', 'integer', Rule::exists('roles', 'id')]
        ];
    }

    public function validated(): array
    {
        if ($this->has('unidade')) {
            return array_merge(parent::validated(), [
                'tenant_id' => $this->input('unidade')
            ]);
        }

        return parent::validated();
    }
}
