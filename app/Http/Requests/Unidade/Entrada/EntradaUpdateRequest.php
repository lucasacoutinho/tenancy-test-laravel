<?php

namespace App\Http\Requests\Unidade\Entrada;

use Domain\Status\Status;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesEntradas;
use Illuminate\Foundation\Http\FormRequest;

class EntradaUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesEntradas::UPDATE);
    }

    public function rules()
    {
        return [
            'status' => ['filled', 'string', Rule::in([Status::CONFIRMADA, Status::REPROVADA])]
        ];
    }
}
