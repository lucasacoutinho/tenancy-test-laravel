<?php

namespace App\Http\Controllers\Unidade\Entrada;

use Inertia\Inertia;
use Illuminate\Support\Str;
use App\Models\Transferencia;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesEntradas;
use App\Http\Requests\Unidade\Entrada\EntradaUpdateRequest;

class EntradaController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesEntradas::INDEX), 403);

        $transferencias = Transferencia::where('unidade_destino', tenant('id'))->orderByDesc('created_at')->get()->map(function ($transferencia) {
            return [
                'id'              => $transferencia->id,
                'unidade_origem'  => $transferencia->unidade_origem,
                'unidade_destino' => $transferencia->unidade_destino,
                'ativo_id'        => $transferencia->ativo_id,
                'quantidade'      => $transferencia->quantidade,
                'status'          => $transferencia->status,
                'created_at'      => $transferencia->created_at->format('d-m-Y H:i:s'),
                'updated_at'      => $transferencia->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Unidade/Entrada/Index', [
            'transferencias' => $transferencias,
            'permissoes' => [
                'store'   => PermissoesEntradas::STORE,
                'update'  => PermissoesEntradas::UPDATE,
                'destroy' => PermissoesEntradas::DESTROY,
            ]
        ]);
    }

    public function update(EntradaUpdateRequest $request, Transferencia $entrada)
    {
        DB::transaction(function () use ($request, $entrada) {
            $entrada->update($request->validated());
        });

        return Redirect::route('unidade.entradas.index')->with('success', Str::replace('{TEMP}', Str::lower($request->status), 'Entrada {TEMP} com sucesso!'));
    }
}
