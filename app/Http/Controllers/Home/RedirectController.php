<?php

namespace App\Http\Controllers\Home;

use Inertia\Inertia;
use App\Models\Tenant;
use App\Http\Controllers\Controller;

class RedirectController extends Controller
{
    public function index()
    {
        $unidades = Tenant::query()->with('domains')->get()->map(function ($unidade) {
            return [
                'unidade' => $unidade->id,
                'url'     => $unidade->domains()->first()->domain,
            ];
        });

        return Inertia::render('Home/Index', [
            'unidades' => $unidades
        ]);
    }
}
