<?php

namespace App\Http\Controllers\Central\Compra;

use Inertia\Inertia;
use App\Models\Fornecedor;
use App\Models\NotaFiscal;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesNotasFiscais;
use App\Http\Requests\Central\Compra\NotaFiscalStoreRequest;
use App\Http\Requests\Central\Compra\NotaFiscalUpdateRequest;

class NotaFiscalController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesNotasFiscais::INDEX), 403);

        $fornecedores = Fornecedor::all()->map(function($fornecedor) {
            return [
                'id'     => $fornecedor->id,
                'titulo' => $fornecedor->titulo
            ];
        });

        $notasFiscais = NotaFiscal::all()->map(function($notaFiscal) {
            return [
                'id'             => $notaFiscal->id,
                'nota_fiscal'    => $notaFiscal->numero_nota_fiscal,
                'fornecedor'     => $notaFiscal->fornecedor()->first(),
                'comprado_at'    => $notaFiscal->comprado_at->format('d-m-Y'),
                'created_at'     => $notaFiscal->created_at->format('d-m-Y H:i:s'),
                'updated_at'     => $notaFiscal->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Central/NotaFiscal/Index', [
            'notasFiscais' => $notasFiscais,
            'fornecedores' => $fornecedores,
            'permissoes' => [
                'store'   => PermissoesNotasFiscais::STORE,
                'update'  => PermissoesNotasFiscais::UPDATE,
                'destroy' => PermissoesNotasFiscais::DESTROY,
            ]
        ]);
    }

    public function store(NotaFiscalStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $notaFiscal = NotaFiscal::create($request->validated());

            return $notaFiscal;
        });

        return Redirect::route('central.notas-fiscais.index')->with('success', 'Nota fiscal criada com sucesso!');
    }

    public function update(NotaFiscalUpdateRequest $request, NotaFiscal $notasFiscai)
    {
        DB::transaction(function () use ($request, $notasFiscai) {
            $notasFiscai->update($request->validated());
        });

        return Redirect::route('central.notas-fiscais.index')->with('success', 'Nota fiscal atualizada com sucesso!');
    }

    public function destroy(NotaFiscal $notasFiscai)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesNotasFiscais::DESTROY), 403);

        if($notasFiscai->ativos()->get()->IsNotEmpty()) {
            return Redirect::route('central.notas-fiscais.index')->with('error', 'Não é possível excluir nota fiscal que pertence à ativos!');
        }

        $notasFiscai->delete();

        return Redirect::route('central.notas-fiscais.index')->with('success', 'Nota fiscal excluida com sucesso!');
    }
}
