<?php

namespace App\Http\Controllers\Central\Unidade;

use Inertia\Inertia;
use App\Models\Tenant;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesUnidades;
use App\Http\Requests\Central\Unidade\UnidadeStoreRequest;
use App\Http\Requests\Central\Unidade\UnidadeUpdateRequest;

class UnidadeController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesUnidades::INDEX), 403);

        $unidades = Tenant::with('domains')->orderByDesc('created_at')->get()->map(function ($unidade) {
            return [
                'unidade'    => $unidade->id,
                'dominio'    => $unidade->domains()->first()->domain,
                'created_at' => $unidade->created_at->format('d-m-Y H:i:s'),
                'updated_at' => $unidade->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Central/Unidade/Index', [
            'unidades'   => $unidades,
            'permissoes' => [
                'store'   => PermissoesUnidades::STORE,
                'update'  => PermissoesUnidades::UPDATE,
                'destroy' => PermissoesUnidades::DESTROY,
            ]
        ]);
    }

    public function store(UnidadeStoreRequest $request)
    {
        $unidade = DB::transaction(function () use ($request) {
            $unidade = Tenant::create($request->validated());

            $unidade->domains()->create([
                'domain' => Str::of(config('app.base_url'))->append('/')->append('unidade')->append('/')->append(Str::of($unidade->id)->kebab())->__toString()
            ]);

            return $unidade;
        });

        return Redirect::route('central.unidades.index')->with('success', 'Unidade criada com sucesso!');
    }

    public function update(UnidadeUpdateRequest $request, Tenant $unidade)
    {
        $unidade = DB::transaction(function () use ($request, $unidade) {
            $unidade->update($request->validated());

            $unidade->domains()->first()->update([
                'domain' => Str::of(config('app.base_url'))->append('/')->append('unidade')->append('/')->append(Str::of($unidade->id)->kebab())->__toString()
            ]);

            return $unidade;
        });

        return Redirect::route('central.unidades.index')->with('success', 'Unidade atualizada com sucesso!');
    }

    public function destroy(Tenant $unidade)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesUnidades::DESTROY), 403);

        if($unidade->usuarios()->get()->isNotEmpty()) {
            return Redirect::route('central.unidades.index')->with('error', 'Não é possível excluir uma unidade com usuários!');
        }

        if($unidade->ativos()->get()->isNotEmpty()) {
            return Redirect::route('central.unidades.index')->with('error', 'Não é possível excluir uma unidade com ativos!');
        }

        if($unidade->trasnferenciasEnviadas()->get()->isNotEmpty()) {
            return Redirect::route('central.unidades.index')->with('error', 'Não é possível excluir uma unidade que tenha realizado transferência de itens a outra unidade!');
        }

        if($unidade->transferenciasRecebidas()->get()->isNotEmpty()) {
            return Redirect::route('central.unidades.index')->with('error', 'Não é possível excluir uma unidade que tenha recebido transferência de itens de outra unidade!');
        }

        $unidade->delete();

        return Redirect::route('central.unidades.index')->with('success', 'Unidade excluida com sucesso!');
    }
}
