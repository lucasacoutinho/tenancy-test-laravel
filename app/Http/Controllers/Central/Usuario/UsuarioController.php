<?php

namespace App\Http\Controllers\Central\Usuario;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Tenant;
use Domain\Funcoes\Funcoes;
use App\Models\Role as Funcao;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesUsuarios;
use App\Http\Requests\Central\Usuario\UsuarioStoreRequest;
use App\Http\Requests\Central\Usuario\UsuarioUpdateRequest;

class UsuarioController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesUsuarios::INDEX), 403);

        $unidades = Tenant::all(['id']);

        $usuarios = User::with('tenant')->orderByDesc('created_at')->get()->map(function ($usuario) {
            return [
                'id'         => $usuario->id,
                'nome'       => $usuario->name,
                'email'      => $usuario->email,
                'unidade'    => $usuario->tenant()->exists() ? $usuario->tenant()->first()->id : null,
                'funcao'     => $usuario->roles()->get()->isNotEmpty() ? $usuario->roles()->first()->only(['id', 'name']) : null,
                'created_at' => $usuario->created_at->format('d-m-Y H:i:s'),
                'updated_at' => $usuario->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        $funcoes = Funcao::all()->map(function ($funcao) {
            return [
                'id'     => $funcao->id,
                'funcao' => $funcao->name
            ];
        });

        return Inertia::render('Central/Usuario/Index', [
            'usuarios' => $usuarios,
            'unidades' => $unidades,
            'funcoes'  => $funcoes,
            'permissoes' => [
                'store'   => PermissoesUsuarios::STORE,
                'update'  => PermissoesUsuarios::UPDATE,
                'destroy' => PermissoesUsuarios::DESTROY,
            ]
        ]);
    }

    public function store(UsuarioStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $usuario = User::create($request->validated());

            if($request->input('funcao')) {
                $usuario->syncRoles($request->input('funcao'));
            }

            return $usuario;
        });

        return Redirect::route('central.usuarios.index')->with('success', 'Usuário criado com sucesso!');
    }

    public function update(UsuarioUpdateRequest $request, User $usuario)
    {
        if(!authenticatedUserIsAdmin() && $request->input('funcao') === roleAdminID()) {
            return Redirect::route('central.usuarios.index')->with('success', 'Não foi possível atualizar a função do usuário para administrador!');
        }

        if(!authenticatedUserHasRoles() && $request->input('funcao')) {
            return Redirect::route('central.usuarios.index')->with('success', 'Não foi possível atualizar a função do usuário!');
        }

        DB::transaction(function () use ($request, $usuario) {
            $usuario->update($request->validated());

            if ($request->input('unidade')) {
                $usuario->unsetRelation('tenant');
                $usuario->setRelation('tenant', $request->input('unidade'));
                $usuario->save();
            }

            if(authenticatedUserIsAdmin()){
                $usuario->syncRoles($request->input('funcao'));
            } else if ($request->input('funcao') !== roleAdminID()) {
                $usuario->syncRoles($request->input('funcao'));
            }

            return $usuario;
        });

        return Redirect::route('central.usuarios.index')->with('success', 'Usuário atualizado com sucesso!');
    }

    public function destroy(User $usuario)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesUsuarios::DESTROY), 403);

        if($usuario->hasRole(Funcoes::ADMINISTRADOR)) {
            return Redirect::route('central.usuarios.index')->with('error', 'Não é possível excluir usuário administrador!');
        }

        if($usuario === getAuthenticatedUser()) {
            return Redirect::route('central.usuarios.index')->with('error', 'Não é possível excluir o proprio usuário!');
        }

        $usuario->delete();

        return Redirect::route('central.usuarios.index')->with('success', 'Usuário excluido com sucesso!');
    }
}
