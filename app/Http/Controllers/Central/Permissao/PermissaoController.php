<?php

namespace App\Http\Controllers\Central\Permissao;

use Inertia\Inertia;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesPermissoes;
use App\Http\Requests\Central\Permissao\PermissaoStoreRequest;
use App\Http\Requests\Central\Permissao\PermissaoUpdateRequest;

class PermissaoController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesPermissoes::INDEX), 403);

        $permissoes = Permission::orderByDesc('created_at')->get()->map(function ($permissao) {
            return [
                'id'         => $permissao->id,
                'nome'       => $permissao->name,
                'guard'      => $permissao->guard_name,
                'created_at' => $permissao->created_at->format('d-m-Y H:i:s'),
                'updated_at' => $permissao->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Central/Permissao/Index', [
            'permissoes'           => $permissoes,
            'permissoesAtribuidas' => [
                'store'   => PermissoesPermissoes::STORE,
                'update'  => PermissoesPermissoes::UPDATE,
                'destroy' => PermissoesPermissoes::DESTROY,
            ]
        ]);
    }

    public function store(PermissaoStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $permissao = Permission::create($request->validated());

            return $permissao;
        });

        return Redirect::route('central.permissoes.index')->with('success', 'Permissão criada com sucesso!');
    }

    public function update(PermissaoUpdateRequest $request, Permission $permissao)
    {
        DB::transaction(function () use ($request, $permissao) {
            $permissao->update($request->validated());

            return $permissao;
        });

        return Redirect::route('central.permissoes.index')->with('success', 'Permissão atualizada com sucesso!');
    }

    public function destroy(Permission $permissao)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesPermissoes::DESTROY), 403);

        $permissao->delete();

        return Redirect::route('central.permissoes.index')->with('success', 'Permissão excluida com sucesso!');
    }
}
