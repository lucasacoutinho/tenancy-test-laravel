<?php

namespace App\Http\Controllers\Central\Ativo;

use Inertia\Inertia;
use App\Models\Ativo;
use App\Models\Categoria;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Domain\Permissoes\PermissoesAtivos;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Central\Ativo\AtivoStoreRequest;
use App\Http\Requests\Central\Ativo\AtivoUpdateRequest;

class AtivoController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesAtivos::INDEX), 403);

        $categorias = Categoria::all()->map(function ($categoria) { return ['id' => $categoria->id, 'titulo' => $categoria->titulo];});

        $ativos = Ativo::with(['detalhe', 'categorias'])->orderByDesc('created_at')->get()->map(function ($ativo) {
            return [
                'id'                 => $ativo->id,
                'titulo'             => $ativo->titulo,
                'descricao'          => Str::limit($ativo->descricao, 20, '...'),
                'n_patrimonio'       => $ativo->detalhe->n_patrimonio,
                'n_serie'            => $ativo->detalhe->n_serie,
                'n_etiqueta_servico' => $ativo->detalhe->n_etiqueta_servico,
                'categorias'         => $ativo->categorias->pluck('id'),
                'created_at'         => $ativo->created_at->format('d-m-Y H:i:s'),
                'updated_at'         => $ativo->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Central/Ativo/Index', [
            'ativos'     => $ativos,
            'categorias' => $categorias,
            'permissoes' => [
                'store'   => PermissoesAtivos::STORE,
                'update'  => PermissoesAtivos::UPDATE,
                'destroy' => PermissoesAtivos::DESTROY,
            ]
        ]);
    }

    public function store(AtivoStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $ativo = Ativo::create($request->validated());

            if($request->input('categorias')){
                $ativo->categorias()->attach($request->input('categorias'));
            }

            $ativo->detalhe()->create($request->validated());
        });

        return Redirect::route('central.ativos.index')->with('success', 'Ativo criado com sucesso!');
    }

    public function update(AtivoUpdateRequest $request, Ativo $ativo)
    {
        DB::transaction(function () use ($request, $ativo) {
            $ativo->update($request->validated());

            if($request->input('categorias')){
                $ativo->categorias()->sync($request->input('categorias'));
            } else {
                $ativo->categorias()->detach();
            }

            $ativo->detalhe()->first()->update($request->validated());
        });

        return Redirect::route('central.ativos.index')->with('success', 'Ativo atualizado com sucesso!');
    }

    public function destroy(Ativo $ativo)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesAtivos::DESTROY), 403);

        if($ativo->compras()->get()->isNotEmpty()) {
            return Redirect::route('central.ativos.index')->with('error', 'Não é possível excluir um ativo com histórico de compras!');
        }

        if($ativo->unidades()->get()->isNotEmpty()) {
            return Redirect::route('central.ativos.index')->with('error', 'Não é possível excluir um ativo que pertence á uma unidade!');
        }

        if($ativo->transferencias()->get()->isNotEmpty()) {
            return Redirect::route('central.ativos.index')->with('error', 'Não é possível excluir um ativo com histórico de transferências!');
        }

        $ativo->delete();

        return Redirect::route('central.ativos.index')->with('success', 'Ativo excluido com sucesso!');
    }
}
