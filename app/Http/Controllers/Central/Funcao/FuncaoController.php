<?php

namespace App\Http\Controllers\Central\Funcao;

use App\Models\Role;
use Inertia\Inertia;
use App\Models\Permission;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Domain\Permissoes\PermissoesFuncoes;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Central\Funcao\FuncaoStoreRequest;
use App\Http\Requests\Central\Funcao\FuncaoUpdateRequest;

class FuncaoController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesFuncoes::INDEX), 403);

        $permissoes =  Permission::all()->map(function ($permissao) {
            return [
                'id'     => $permissao->id,
                'nome'   => $permissao->name
            ];
        });

        $funcoes = Role::with(['permissions'])->get()->map(
            function ($funcao) {
                return [
                    'id'         => $funcao->id,
                    'nome'       => $funcao->name,
                    'guard'      => $funcao->guard_name,
                    'permissoes' => $funcao->permissions()->get()->pluck('id')->toArray(),
                    'created_at' => $funcao->created_at->format('d-m-Y H:i:s'),
                    'updated_at' => $funcao->updated_at->format('d-m-Y H:i:s'),
                ];
            }
        );

        return Inertia::render('Central/Funcoes/Index', [
            'funcoes'              => $funcoes,
            'permissoes'           => $permissoes,
            'permissoesAtribuidas' => [
                'store'   => PermissoesFuncoes::STORE,
                'update'  => PermissoesFuncoes::UPDATE,
                'destroy' => PermissoesFuncoes::DESTROY,
            ]
        ]);
    }

    public function store(FuncaoStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $funcao = Role::create(Arr::except($request->validated(), ['permissions']));

            $funcao->syncPermissions($request->input('permissoes'));
        });

        return Redirect::route('central.funcoes.index')->with('success', 'Função criada com sucesso!');
    }

    public function update(FuncaoUpdateRequest $request, Role $funcao)
    {
        DB::transaction(function () use ($request, $funcao) {
            $funcao->update(Arr::except($request->validated(), ['permissions']));

            $funcao->syncPermissions($request->input('permissoes'));
        });

        return Redirect::route('central.funcoes.index')->with('success', 'Função atualizada com sucesso!');
    }

    public function destroy(Role $funcao)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesFuncoes::DESTROY), 403);
        abort_unless($funcao->users()->get()->isEmpty(), 403, 'Função possui usuários vinculados!');

        $funcao->delete();

        return Redirect::route('central.funcoes.index')->with('success', 'Função excluida com sucesso!');
    }
}
