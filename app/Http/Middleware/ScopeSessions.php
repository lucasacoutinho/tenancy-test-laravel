<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ScopeSessions
{
    public static $tenantIdKey = '_tenant_id';

    public function handle(Request $request, Closure $next)
    {
        if (! $request->session()->has(static::$tenantIdKey)) {
            $request->session()->put(static::$tenantIdKey, (!!tenant() ? tenant()->getTenantKey() : null));
        } else {
            if ($request->session()->get(static::$tenantIdKey) !== (!!tenant() ? tenant()->getTenantKey() : null)) {
                $request->session()->invalidate();
            }
        }

        return $next($request);
    }
}
