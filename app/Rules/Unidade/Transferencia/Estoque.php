<?php

namespace App\Rules\Unidade\Transferencia;

use Domain\Status\Status;
use Illuminate\Contracts\Validation\Rule;
use App\Models\AtivoUnidade as EstoqueUnidade;
use App\Models\Transferencia as TransferenciaPendente;

class Estoque implements Rule
{
    private $ativo;
    private $unidade;
    private $transferencia;

    public function __construct($ativo, $unidade, $transferencia = null)
    {
        $this->ativo         = $ativo;
        $this->unidade       = $unidade;
        $this->transferencia = $transferencia;
    }

    public function passes($attribute, $value)
    {
        return $value <= $this->calcularEstoqueAtual();
    }

    private function calcularEstoqueTotal()
    {
        return EstoqueUnidade::query()
            ->where('tenant_id', $this->unidade)
            ->where('ativo_id', $this->ativo)
            ->sum('quantidade');
    }

    private function calcularEstoqueUtilizado()
    {
        $estoqueEmTransferencia = TransferenciaPendente::query()
            ->when($this->transferencia, function ($query) {
                return $query->where('id', '!=', $this->transferencia);
            })
            ->where('unidade_origem', $this->unidade)
            ->where('ativo_id', $this->ativo)
            ->where('status', Status::PENDENTE)
            ->sum('quantidade');

        return $estoqueEmTransferencia;
    }

    private function calcularEstoqueAtual()
    {
        return $this->calcularEstoqueTotal() - $this->calcularEstoqueUtilizado();
    }

    public function message()
    {
        return 'The :attribute value must be less or equal to the current stock.';
    }
}
