### Requerimentos
- PHP 7.4
- Composer
- MySQL
- NodeJS


### Instalando o Projeto
- Baixa o projeto do gitlab
```
git clone https://gitlab.com/lucasacoutinho/sistema-de-controle-de-estoque
```
- Entre no diretório do projeto
```
cd sistema-de-controle-de-estoque
```
- Instale as dependencias do projeto
```
composer install
```
```
npm install
```
- Crie o banco de dados
```
mysql -u root -p
```
```
CREATE DATABASE IF NOT EXISTS `laravel`;
```

- Copie o arquivo .env.example para .env
```
cp .env.example .env
```

- Gere uma chave para a aplicação laravel
```
php artisan key:generate
```

- Inicie o servidor
```
php artisan serve
```

- Compile os assets
```
npm run prod
```

- Acesse o site
```
http://localhost:8000
```
