<?php
namespace Domain\Status;

class Status {
    public const PENDENTE    = 'PENDENTE';
    public const REPROVADA   = 'REPROVADA';
    public const CONFIRMADA  = 'CONFIRMADA';
}
