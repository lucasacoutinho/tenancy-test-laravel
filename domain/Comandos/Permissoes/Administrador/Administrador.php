<?php

namespace Domain\Comandos\Permissoes\Administrador;

use App\Models\User;
use Domain\Funcoes\Funcoes;

class Administrador
{
    private $id;
    private const FUNCAO = Funcoes::ADMINISTRADOR;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    private function usuario(): User
    {
        return User::findOrFail($this->id);
    }

    public function atribuir()
    {
        $this->usuario()->assignRole(self::FUNCAO);

        return $this;
    }

    public function remover()
    {
        $this->usuario()->removeRole(self::FUNCAO);

        return $this;
    }
}
