<?php

namespace Domain\Permissoes;

class PermissoesAtivos
{
    public const INDEX   = 'ativo-index';
    public const STORE   = 'ativo-store';
    public const UPDATE  = 'ativo-update';
    public const DESTROY = 'ativo-destroy';
}
