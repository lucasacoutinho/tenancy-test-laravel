<?php

namespace Domain\Permissoes;

class PermissoesUnidades
{
    public const INDEX   = 'unidade-index';
    public const STORE   = 'unidade-store';
    public const UPDATE  = 'unidade-update';
    public const DESTROY = 'unidade-destroy';
}
