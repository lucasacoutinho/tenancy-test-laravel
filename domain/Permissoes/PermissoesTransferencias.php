<?php

namespace Domain\Permissoes;

class PermissoesTransferencias
{
    public const INDEX   = 'transferencia-index';
    public const STORE   = 'transferencia-store';
    public const UPDATE  = 'transferencia-update';
    public const DESTROY = 'transferencia-destroy';
}
