<?php

namespace Domain\Permissoes;

class PermissoesCategorias
{
    public const INDEX   = 'categoria-index';
    public const STORE   = 'categoria-store';
    public const UPDATE  = 'categoria-update';
    public const DESTROY = 'categoria-destroy';
}
