<?php

namespace Domain\Permissoes;

class PermissoesCompras
{
    public const INDEX   = 'compra-index';
    public const STORE   = 'compra-store';
    public const UPDATE  = 'compra-update';
    public const DESTROY = 'compra-destroy';
}
