<?php

namespace Domain\Permissoes;

class PermissoesFornecedores
{
    public const INDEX   = 'fornecedor-index';
    public const STORE   = 'fornecedor-store';
    public const UPDATE  = 'fornecedor-update';
    public const DESTROY = 'fornecedor-destroy';
}
