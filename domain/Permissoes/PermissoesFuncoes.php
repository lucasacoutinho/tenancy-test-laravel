<?php

namespace Domain\Permissoes;

class PermissoesFuncoes
{
    public const INDEX   = 'funcao-index';
    public const STORE   = 'funcao-store';
    public const UPDATE  = 'funcao-update';
    public const DESTROY = 'funcao-destroy';
}
