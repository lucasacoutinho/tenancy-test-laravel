<?php

namespace Domain\Ativo;

class Estado
{
    public const NOVO         = 'novo';
    public const FIM_DE_USO   = 'fim de uso';
    public const PRE_OBSOLETO = 'pre-obsoleto';
    public const OBSOLETO     = 'obsoleto';
    public const INSERVIVEL   = 'inservivel';
}
