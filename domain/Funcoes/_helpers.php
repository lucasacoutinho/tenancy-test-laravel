<?php

use Domain\Funcoes\Funcoes;
use Spatie\Permission\Models\Role;

if (!function_exists('authenticatedUserIsAdmin')) {
    function authenticatedUserIsAdmin(): bool
    {
        return getAuthenticatedUser()->hasRole(Role::where('name', Funcoes::ADMINISTRADOR)->firstOrFail()) ? true : false;
    }
}

if (!function_exists('authenticatedUserHasRoles')) {
    function authenticatedUserHasRoles(): bool
    {
        return getAuthenticatedUser()->roles()->get()->isNotEmpty();
    }
}

if (!function_exists('roleAdminID')) {
    function roleAdminID(): int
    {
        return Role::findByName(Funcoes::ADMINISTRADOR)->id;
    }
}
