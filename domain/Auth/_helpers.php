<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;

if (!function_exists('getAuthenticatedUser')) {
    function getAuthenticatedUser(): ?User
    {
        return Auth::guard('web')->user();
    }
}
