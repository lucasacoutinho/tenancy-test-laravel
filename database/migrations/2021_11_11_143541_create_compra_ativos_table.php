<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraAtivosTable extends Migration
{
    public function up()
    {
        Schema::create('compra_ativos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ativo_id')->constrained('ativos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('nota_fiscal_id')->constrained('compra_nota_fiscal')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger('quantidade');
            $table->float('valor_unitario');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('compra_ativos');
    }
}
