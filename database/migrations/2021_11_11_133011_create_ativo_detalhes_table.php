<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtivoDetalhesTable extends Migration
{
    public function up()
    {
        Schema::create('ativo_detalhes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ativo_id')->constrained('ativos')->onUpdate('cascade')->onDelete('cascade');
            $table->string('n_patrimonio')->nullable();
            $table->string('n_serie')->nullable();
            $table->string('n_etiqueta_servico')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ativo_detalhes');
    }
}
