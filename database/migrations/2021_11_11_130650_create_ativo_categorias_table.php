<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtivoCategoriasTable extends Migration
{
    public function up()
    {
        Schema::create('ativo_categorias', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ativo_id')->constrained('ativos', 'id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('categoria_id')->constrained('categorias', 'id')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ativo_categorias');
    }
}
