<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantAtivosTable extends Migration
{
    public function up()
    {
        Schema::create('tenant_ativos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ativo_id')->constrained('ativos')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger('quantidade');
            $table->timestamps();

            $table->string('tenant_id')->nullable();
            $table->foreign('tenant_id')->references('id')->on('tenants')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('tenant_ativos');
    }
}
