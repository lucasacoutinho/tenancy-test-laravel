<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferenciasTable extends Migration
{
    public function up()
    {
        Schema::create('transferencias', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ativo_id')->constrained('ativos')->onUpdate('cascade')->onDelete('cascade');

            $table->string('unidade_origem')->nullable();
            $table->foreign('unidade_origem')->references('id')->on('tenants')->onUpdate('cascade')->onDelete('cascade');

            $table->string('unidade_destino');
            $table->foreign('unidade_destino')->references('id')->on('tenants')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedBigInteger('quantidade');
            $table->string('status');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('transferencias');
    }
}
