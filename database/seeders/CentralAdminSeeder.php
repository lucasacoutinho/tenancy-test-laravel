<?php

namespace Database\Seeders;

use App\Models\User;
use Domain\Funcoes\Funcoes;
use Illuminate\Database\Seeder;

class CentralAdminSeeder extends Seeder
{
    public function run()
    {
        $this->criarAdministrador();
    }

    private function criarUsuario(): User
    {
        return User::factory()->admin()->create();
    }

    private function criarAdministrador()
    {
        $this->criarUsuario()->assignRole(Funcoes::ADMINISTRADOR);
    }
}
