<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Domain\Permissoes\PermissoesAtivos;
use Domain\Permissoes\PermissoesCompras;
use Domain\Permissoes\PermissoesFuncoes;
use Domain\Permissoes\PermissoesEntradas;
use Domain\Permissoes\PermissoesUnidades;
use Domain\Permissoes\PermissoesUsuarios;
use Domain\Permissoes\PermissoesCategorias;
use Domain\Permissoes\PermissoesPermissoes;
use Domain\Permissoes\PermissoesFornecedores;
use Domain\Permissoes\PermissoesNotasFiscais;
use Domain\Permissoes\PermissoesTransferencias;

class PermissionSeeder extends Seeder
{
    private const GUARD = 'web';

    public function run()
    {
        $this->criarPermissoes();
    }

    private function criarPermissoes()
    {
        $this->permissoesAtivos();
        $this->permissoesCompras();
        $this->permissoesEntradas();
        $this->permissoesUnidades();
        $this->permissoesUsuarios();
        $this->permissoesCategorias();
        $this->permissoesFornecedores();
        $this->permissoesTransferencias();
        $this->permissoesPermissoes();
        $this->permissoesFuncoes();
        $this->permissoesNotasFiscais();
    }

    private function permissoesAtivos()
    {
        $permissoes = [
            PermissoesAtivos::INDEX,
            PermissoesAtivos::STORE,
            PermissoesAtivos::UPDATE,
            PermissoesAtivos::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesCategorias()
    {
        $permissoes = [
            PermissoesCategorias::INDEX,
            PermissoesCategorias::STORE,
            PermissoesCategorias::UPDATE,
            PermissoesCategorias::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesCompras()
    {
        $permissoes = [
            PermissoesCompras::INDEX,
            PermissoesCompras::STORE,
            PermissoesCompras::UPDATE,
            PermissoesCompras::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesEntradas()
    {
        $permissoes = [
            PermissoesEntradas::INDEX,
            PermissoesEntradas::STORE,
            PermissoesEntradas::UPDATE,
            PermissoesEntradas::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesFornecedores()
    {
        $permissoes = [
            PermissoesFornecedores::INDEX,
            PermissoesFornecedores::STORE,
            PermissoesFornecedores::UPDATE,
            PermissoesFornecedores::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesTransferencias()
    {
        $permissoes = [
            PermissoesTransferencias::INDEX,
            PermissoesTransferencias::STORE,
            PermissoesTransferencias::UPDATE,
            PermissoesTransferencias::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesUnidades()
    {
        $permissoes = [
            PermissoesUnidades::INDEX,
            PermissoesUnidades::STORE,
            PermissoesUnidades::UPDATE,
            PermissoesUnidades::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesUsuarios()
    {
        $permissoes = [
            PermissoesUsuarios::INDEX,
            PermissoesUsuarios::STORE,
            PermissoesUsuarios::UPDATE,
            PermissoesUsuarios::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesPermissoes()
    {
        $permissoes = [
            PermissoesPermissoes::INDEX,
            PermissoesPermissoes::STORE,
            PermissoesPermissoes::UPDATE,
            PermissoesPermissoes::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesFuncoes()
    {
        $permissoes = [
            PermissoesFuncoes::INDEX,
            PermissoesFuncoes::STORE,
            PermissoesFuncoes::UPDATE,
            PermissoesFuncoes::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function permissoesNotasFiscais()
    {
        $permissoes = [
            PermissoesNotasFiscais::INDEX,
            PermissoesNotasFiscais::STORE,
            PermissoesNotasFiscais::UPDATE,
            PermissoesNotasFiscais::DESTROY,
        ];

        foreach ($permissoes as $permissao) {
            $this->criarPermissao($permissao);
        }
    }

    private function criarPermissao($permissao)
    {
        $permissao = Permission::create([
            'name'       => $permissao,
            'guard_name' => self::GUARD
        ]);

        return $permissao;
    }
}
