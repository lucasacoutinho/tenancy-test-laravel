<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Permission;
use Domain\Funcoes\Funcoes;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    private const GUARD = 'web';

    public function run()
    {
        $this->criarFuncoes();
    }

    public function criarFuncoes()
    {
        $this->funcaoAdministrador();
    }

    public function funcaoAdministrador()
    {
        $funcao = $this->criarFuncao(Funcoes::ADMINISTRADOR);
        $funcao->givePermissionTo(Permission::all());
    }

    public function criarFuncao($funcao)
    {
        $funcao = Role::create([
            'name'       => $funcao,
            'guard_name' => self::GUARD
        ]);

        return $funcao;
    }
}
