module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/recommended', // Use this if you are using Vue.js 2.x.
    'prettier',
  ],
  plugins: [],
  // add your custom rules here
  rules: {},
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'vue/require-default-prop': 'off',
      },
    },
  ],
}
