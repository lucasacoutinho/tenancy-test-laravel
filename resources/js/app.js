import Vue from 'vue'
import { InertiaProgress } from '@inertiajs/progress'
import { createInertiaApp } from '@inertiajs/inertia-vue'
import vuetify from '@/plugins/vuetify'

Vue.mixin({ methods: { route: window.route } });

Vue.mixin({
  methods: {
    verificarPermissao: function (permissoes, permissao) {
      return permissoes.includes(permissao);
    }
  }
})

createInertiaApp({
  title: (title) => `${title}`,
  resolve: (name) => require(`@/pages/${name}`),
  setup({ el, App, props }) {
    new Vue({
      vuetify,
      render: (h) => h(App, props),
    }).$mount(el)
  },
})

InertiaProgress.init()
